#!/usr/local/bin/bash

#global variables

reload=0			# option to reload named
verbose=0		# option verbose
fzone_ok=0		# If not find Zone Filename
fzone=""			# Zone file
fkey=""			# Key zone file
named_dir="/usr/local/etc/namedb"	# Default directory to Bind
tmp_dir="$named_dir/tmp"				# TMP directory
src_dir="$named_dir/originals"			# Origim Directory
dst_dir="$named_dir/working"			# Destination Directory
sname=`basename $0`
serial_fname="$dst_dir/serial.txt"	# File to store serial number
backup_dir="$dst_dir/backup"			# Default baclup directory


help(){
			echo "Use: $sname -z ZONE_NAME [-h -v -r]"
			echo "  -h       This help message"
			echo "  -r       To reload DNS server"
			echo "  -v       Turn on verbose"
			echo "  -z ZONE  DNS zone to sign"
}

backup(){
	echo "Create backupi $1"

	if [ -e $1 ]; then
		echo "Backup directory exist. Moving old directories "
		for s in {6..1}
		do
			bsrc_dir="$1.$s"
			(( d = s + 1 ))
			bdst_dir="$1.$d"
			mv $bsrc_dir $bdst_dir
		done
		bdst_dir="$1.1"
		mv $1 $bdst_dir
	fi
	mkdir $1
	bfile="$1/backup.tgz"
	tar -czf $bfile *.db 
}

getopt(){
	while [ $1 ]; 
	do
		case $1 in

			"-b")
				bdir=${2:-$backup_dir}
				echo "BACKUP DIR $bdir"
				backup $bdir
				shift
				;;
			"-h")
				help  $0
				;;
			"-r")
				echo "RELOAD on"
				reload=1
				;;
			"-v")
				echo "VERBOSE on"
				verbose=1
				;;
			"-z")
				if grep -Eq '([[:alnum:]]{3}\.){2}[[:alnum:]]{2}' <<< $2 ; then
					if [ $2 ]; then
						zone=$2
						f1="$2.db"
						f2="$src_dir/$f1"
						if [ -e $f1 -o -e $f2 ]; then
							fzone_ok=1
							echo "Signing zone [ $zone ]"
						else
							echo "ERROR: Zone not exist"
						fi
					fi
					shift
				else
					echo "ERROR: Zone file not informed"
					exit
				fi
				;;
		esac
		shift
	done
}


echo "DNSSEC signing zone"

if [ $1 ]; then
	getopt $*
else
	help $0
	exit
fi

fzone="$zone.db"
kfile="K$zone*.key"
src_fzone="$src_dir/$fzone"

if [ $verbose = 1 ]; then
	echo "Variables:"
	echo "    Verbose  :  $verbose"
	echo "    SNAME    :  $sname"
	echo "    NAMED_DIR:  $named_dir"
	echo "    TMP_DIR  :  $tmp_dir"
	echo "    SRC_DIR  :  $src_dir"
	echo "    DST_DIR  :  $dst_dir"
	echo "    SRC_FILE :  $src_zone"
	echo "    KFILE    :  $kfile"
	echo "    FZONE    :  $fzone"
	echo "    FZONE_OK :  $fzone_ok"
	echo "    FKEY     :  $fkey"
	echo "    SRC_FZONE:  $src_fzone"
fi

if [ $fzone_ok -eq 0 ]; then
	help $0
	exit
fi

echo "Coping $src_fzone to $dst_dir"
cp $src_fzone $dst_dir

serial=`cat $serial_fname`
(( serial += 1 ))
if [ $verbose = 1 ]; then
	echo $serial > $serial_fname
fi

if [ $serial -lt 10 ]; then
	serial="0$serial"
fi
date=`date "+%Y%m%d"`
serial="$date$serial"

if sed -i -e "s/SERIALNUMBER/$serial/" $fzone ;  then
	if [ verbose = 1 ]; then
		echo "Subst ok"
	fi
fi
	
if ls $kfile >&- ; then
	kfiles="K$zone*"
	echo "Keyfile exist. Removing $kfiles"
	rm $kfiles
fi

dfile="dsset*$zone*"
if ls $dfile >&- ; then
	echo "Remove files $dfile"
	rm -rf $dfile >&-
fi

dnssec-keygen -a NSEC3RSASHA1 -b 2048 -n ZONE $zone
dnssec-keygen -f KSK -a NSEC3RSASHA1 -b 4096 -n ZONE $zone

c=1
for file in $(ls $kfile)
do
	if [ $verbose = 1 ]; then
		echo "C = $c = $file"
	fi
	echo "\$INCLUDE $file" >> $fzone
	(( c = c + 1 ))
done


r=` head -c 1000 /dev/random | sha1 | cut -b 1-16 `

if [ $verbose = 1 ]; then
	echo "Random Salt: $r "
fi

dnssec-signzone -A -3 $r  -N INCREMENT -o $zone -t $fzone

if [ $verbose = 1 ]; then
	echo "Reloading zones"
fi
service named reload
