#!/bin/sh 

#
# Function to show a wait cycles ( /-\|/-\| )
#
progress()
{
limit=1800
if [ $# -gt 0 ] ; then
	limit=$1
fi
counter=0
char="/ - \\ | / - \\ |"
tput vi
echo -n "Progress: "
while [ $counter -lt $limit ]; do
	for c in $char ; do
		tput sc
		echo -n $c
		sleep 0.1
		tput el1
		tput rc
		sleep 0.3 
	done
	counter=$((counter + 1))
done
tput ve
echo " "
}

#
# Get all devices
#
devices=`ls /dev/ada?`
#
# Get all driver that not is a SSD
#
disks=""
for disk in $devices ; do
        echo $disk
        type=`smartctl -a $disk | grep SSD`
        if [ "$type." == "." ]; then
                disks="$disks $disk"
        fi
done
echo $disks

#
# Start smarctl in each drive
#
for disk in $disks ; do
	echo "Stating smart test on $disk"
	smartctl -t long $disk >> /dev/null
done

#
# Show Progress to wait time  set thsis to 30 minutes
#
progress 1800

#
# Check if the test finish
#
finish=0
while [ $finish -eq 0 ];do
	tput sc
	counter=0
	for disk in $disks ; do
		s=`smartctl -a "$disk" | grep 'test rem' | awk '{ print $1; } ' `
		if [ "$s." ==  "." ]; then
			counter=$((counter + 1))
		else
			echo "Status do disco [ $disk ] faltam [ $s ]"
		fi
	done
	if [ $counter -eq 2 ]; then
		finish=1
	else
		sleep 60
	fi
	progress 1
	tput rc
done
#
# When finish all test, genarete a new log
#
for disk in $disks ; do
	smartctl -a $disk >> /tmp/testdisks.log
done
echo " "

