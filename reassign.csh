#!/bin/csh

set NAMED_DIR="/usr/local/etc/namedb"
set ZONE_DIR="$NAMED_DIR/working"
set DFLT_DIR="$NAMED_DIR/origins"
set SN=`date "+%Y%m%d"`
set SN_FILE="$NAMED_DIR/serial.txt"

echo "Reassign all zones"

if ( -e $SN_FILE ) then
	set OLDSN = `cat $SN_FILE | cut -b 1-8`
	set OLDVER=`cat $SN_FILE | cut -b 9-10`
else
	set OLDSN=0
	set OLDVER=0
endif

if ( $SN == $OLDSN ) then
	@ OLDVER ++
else
	set OLDVER=0
endif

if ( $OLDVER < 10 ) then
	set OLDVER = "0$OLDVER"
endif

set OLDSN="$SN$OLDVER"
echo $OLDSN > $SN_FILE

echo "Copy default files from $DFLT_DIR"
echo "Destination dir default files to $ZONE_DIR"
echo "Serial filename [ $SN_FILE ] = $OLDSN "
echo "Serial Number [ $SN ] "

cp $DFLT_DIR/* $ZONE_DIR >/dev/null
rm -f $ZONE_DIR/ds* >/dev/null
rm -f $ZONE_DIR/K* >/dev/null

set files=`ls $ZONE_DIR/[a-z]*br.db`
foreach file  ( $files )
	sed -e "s/SERIALNUMBERHERE/$OLDSN/" $file > $file:r.aaa
	mv $file:r.aaa $file
	set f=`basename $file`
	./gendnssec.csh	$f:r
end

set files=`ls $ZONE_DIR/ds*`
foreach file ( $files )
	echo $file
	cat $file
end

